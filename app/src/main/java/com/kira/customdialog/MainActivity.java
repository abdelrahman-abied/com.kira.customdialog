package com.kira.customdialog;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
     Dialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDialog=new Dialog(this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mDialog.setContentView(R.layout.dialog);
        mDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ImageButton backbutton=mDialog.findViewById(R.id.backBtn);
        ImageButton forwardButton=mDialog.findViewById(R.id.forwardBtn);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"backBtn",Toast.LENGTH_LONG).show();

            }
        });
        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"forwordBtn",Toast.LENGTH_LONG).show();

            }
        });
    }

    public void click(View view) {
        mDialog.show();

    }
}
